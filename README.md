# release-on-tag

![Build Status](https://gitlab.inria.fr/gitlabci_gallery/release/release-on-tag/badges/main/pipeline.svg)

Example of a simple release workflow based on tag that :
- publish built artefacts in the Generic Package Registry, (available in [`Packages and registries`/`Package Registry`](https://gitlab.inria.fr/gitlabci_gallery/release/release-on-tag/-/packages)  )
- create a release in gitlab UI, (available in [`Deployments`/`Release`](https://gitlab.inria.fr/gitlabci_gallery/release/release-on-tag/-/releases)  )
- publish in a web site hosted in a separate git repository ( [git](https://gitlab.inria.fr/gitlabci_gallery/release/release-on-tag.gitlabpages.inria.fr) and [associated published version](https://gitlabci_gallery.gitlabpages.inria.fr/release/release-on-tag.gitlabpages.inria.fr))


In  this example, the publication is triggered by pushing a tag in the repository.

The example HelloApp is maven based but the release jobs presented here can be applied to any kind of project/language.

## Overview

The CI of this project and its companion project are organized as follow:

```mermaid
flowchart LR
    
    publish_in_remote_gitlabpages-- "commit" -->pages
    
    subgraph release-on-tag/.gitlab-ci.yml
        start1((.))-- "tag/commit" --> build-job
        build-job-- "tag" -->publish_in_generic_registry
        build-job-- "tag" -->publish_in_remote_gitlabpages
        publish_in_generic_registry-- "tag" -->create_release_job
    end
    subgraph release-on-tag.gitlabpages.inria.fr/.gitlab-ci.yml
        start2((.))-- "commit" --> pages
        pages
    end
```


## Example of use

Creates an annotated tag and push it.
```sh
git tag -a 0.0.1-rc1 -m "my very nice version 0.0.1-rc1"
git push origin 0.0.1-rc1
```

:bulb: Tip: a tag can be moved (with git --force) but in this case not all published artefact are correctly cleaned

## Prerequisite

The "Publication in a separate repository" requires to define an access token with the right to commit in the target repository and declare this token as variable in the calling project.

##  Publication in the Generic Package Registry

The job `publish_in_generic_registry` is in charge of retreiving the artefacts resulting from the previous jobs, create an archive for them and then upload it on gitlab.

In this example we upload the windows exe.


You can control the package name, the version and the file name of the asset in teh registry.

More documentation availble here: https://docs.gitlab.com/ee/user/packages/generic_packages/


Note:  Our example is maven-based, we could also directly publish the jars in the `Maven Package Registry`` that is alos supported by Gitlab. 
We didn't add this task in this `.gitlab-ci.yml` but this maven specific process is well documented in https://docs.gitlab.com/ee/user/packages/maven_repository/

## Creation of the Release in Gitlab UI

The job `create_release_job` creates a release in the gitlab UI.

It also adds a link to the asset uploaded in the Generic Package Registry.

The description of the release is based on the tag message. Whe doing a release, you'll probably need to  adapt it manually using Gitlab UI.

## Publication in a separate repository (typically a group level web site)

In this example, we consider the case where the web site is hosted in a separate project. (as opposed to the example in the gallery)
For example a project hosted at the gitlab group level rather than the gitlab project level.

This structure helps in several situations:
- simplification of the publication of several versions of the build result (binaries, doc, reports,...) without adding new artificial commit in the main project.
- creation of a web site collecting sources from several projects

This job requires to configure the repository hosting the web site to accept commit from our CI.

In gitlab.inria.fr/release-workflow/release-on-tag.gitlabpages.inria.fr configure a role to be able to directly commit in the branch that hosts the web site (by default gitlab protect the main branch by allowing only pull request)


Then create an access token in from the gitlab.inria.fr/release-workflow/release-on-tag.gitlabpages.inria.fr `Settings`/`Access Tokens` with *write_repository* permission and a role able to commit (usually *Maintainer*).
Select an expiration date corresponding to the expected lifetime of your project. Save the value to copy it later.

In the current project, create a **Variable** named `WEB_PUBLISHER_ACCESS_TOKEN`  in CI/CD settings with the above value.
This token will allow the runner to commit in the gitlab.inria.fr/release-workflow/release-on-tag.gitlabpages.inria.fr web site repository.

This variable should be masked for better security.

The publication on the web is actually two folds.  One part is done in this project, but the web site construction itself is delegated to the CI assoc iated to the web site repository.

In this example, it features a simple static site that computes *index.html* files for each folder in order to show their content.

## Advanced

You may move the tag using `-f`

```sh
git tag -a 0.0.1-rc1 -f -m "my very nice version 0.0.1-rc1"
git push origin 0.0.1-rc1 -f
```

However, you need to :
- manually delete the previous release in [`Deployments`/`Release`](https://gitlab.inria.fr/gitlabci_gallery/release/release-on-tag/-/releases)
- make sure to manage duplicated package assets (ie. same name and version) [`Settings`/`Packages and registries`](https://gitlab.inria.fr/gitlabci_gallery/release/release-on-tag/-/settings/packages_and_registries)



