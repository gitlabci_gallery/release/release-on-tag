package fr.inria.hellogit;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }

    @Test
    public void testHelloFR()
    {
        assertEquals( App.helloGit("fr"), "Bonjour git!" );
    }

    @Test
    public void testHelloEO()
    {
        assertEquals( App.helloGit("eo"), "Saluton git!" );
    }
}
