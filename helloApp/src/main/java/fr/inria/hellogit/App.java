package fr.inria.hellogit;

/**
 * Hello Git!
 *
 */
public class App 
{
    
    public static void main( String[] args )
    {
        System.out.println(helloGit(args.length == 1 ? args[0] : "") );
    }
    
    public static String helloGit(String languageCode) {
        switch(languageCode) {
            case "fr": return "Bonjour git!";
            case "en": return "Hello git!";
            case "de": return "Hallo git!";
            case "it": return "Buongiorno git!";
            case "es": return "Buenos dias git!";
            case "eo": return "Saluton git!";
            default: return "Hi git!";
        }
    }
}
